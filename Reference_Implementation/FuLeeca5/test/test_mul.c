#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "../params.h"
#include "../randombytes.h"
#include "../poly.h"
#include "../utils.h"
#include "../fips202.h"

#define NTESTS 10

/*************************************************
* Name:        poly_mul_naive
*
* Description: Naive inplace multiplication of two polnomials
* length N/2, result stored in a
*
* Arguments:   - poly *a: pointer to input/output polynomial
*              - poly *b: pointer to input/output polynomial
**************************************************/
static void poly_schoolbook_circular(poly_n_2 *c, const poly_n_2 *a, const poly_n_2 *b)
{
	for (int i = 0; i < N/2; i++) {
		for (int j = 0; j < N/2 - i; j++) {
			c->coeffs[i + j] = (coeff)(((d_coeff)c->coeffs[i + j] + ((d_coeff)a->coeffs[i] * b->coeffs[j]) % P) % P);
		}
	}
	for (int j = 1; j < N/2; j++) {
		for (int i = N/2 - j; i < N/2; i++) {
			c->coeffs[i + j - N/2] = (coeff)(((d_coeff)c->coeffs[i + j - N/2] + ((d_coeff)a->coeffs[i] * b->coeffs[j]) % P) % P);
		}
	}
}


static void poly_naivemul(poly_n_2 *c, const poly_n_2 *a, const poly_n_2 *b) {
  unsigned int i,j;
  int32_t r[N] = {0};

  for(i = 0; i < N/2; i++)
    for(j = 0; j < N/2; j++)
      r[i+j] = (r[i+j] + (int64_t)a->coeffs[i]*b->coeffs[j]) % P;

  for(i = N/2; i < N; i++)
    r[i-N/2] = (r[i-N/2] + r[i]) % P;

  for(i = 0; i < N/2; i++)
    c->coeffs[i] = r[i];
}

int main(void) {
  unsigned int i, j;
  uint8_t seed[SEEDBYTES];
  uint16_t nonce = 0;
  poly_n_2 a, b, c, d, e = { 0 };
  for(i = 0; i < N/2; i++)
  {
	a.coeffs[i] = 0;
	b.coeffs[i] = 0;
	c.coeffs[i] = 0;
	d.coeffs[i] = 0;
	e.coeffs[i] = 0;
  }

  randombytes(seed, sizeof(seed));
  keccak_state state;

  shake128_init(&state);
  shake128_absorb(&state, seed, SEEDBYTES);
  shake128_finalize(&state);
  poly_sample_from_typical_set(&a, &state);
  poly_sample_from_typical_set(&b, &state);

  for(i = 0; i < NTESTS; ++i) {
    for(j = 0; j < N/2; ++j)
    {
      e.coeffs[j] = 0;
    }
    poly_schoolbook_circular(&e, &a, &b);
    poly_naivemul(&c, &a, &b);
    poly_mul_modp(&d, &a, &b);
    for(j = 0; j < N/2; ++j) {
      if((e.coeffs[j] - c.coeffs[j]) % P)
        fprintf(stderr, "ERROR in multiplication schoolbook: d[%d] = %d != %d\n",j, e.coeffs[j], c.coeffs[j]);
      if((d.coeffs[j] - c.coeffs[j]) % P)
        fprintf(stderr, "ERROR in multiplication karatsuba: d[%d] = %d != %d\n",j, d.coeffs[j], c.coeffs[j]);
      if((d.coeffs[j] - e.coeffs[j]) % P)
        fprintf(stderr, "ERROR in multiplication karatsuba/schoolbook: d[%d] = %d != %d\n",j, d.coeffs[j], e.coeffs[j]);
    }
    memcpy(&b, &a, sizeof(b));
    memcpy(&a, &e, sizeof(a));
  }
  return 0;
}
