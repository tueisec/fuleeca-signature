# FuLeeca

This repository contains the reference implementation of [FuLeeca: A Lee-based Signature Scheme](https://www.ce.cit.tum.de/lnt/forschung/professur-fuer-coding-and-cryptography/fuleeca/ "FuLeeca: A Lee-based Signature Scheme").
